package Calculate;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CalcTest {


    private static ChromeDriver driver;
    @BeforeClass
    public static void initDriver() {
        System.setProperty("webdriver.chrome.driver","/Users/ASUS/Downloads/chromedriver.exe");
        driver=new ChromeDriver();
    }
    @Before
    public void beforeEachTest() {
        driver.get("http://34.90.36.71/apps/sanin/scientificCalculator/index.html");
    }
    @Test
    public void test_OpenCalc(){
        System.setProperty("webdriver.chrome.driver","/Users/ASUS/Downloads/chromedriver.exe");
        ChromeDriver driver=new ChromeDriver();
    }
    @Test
    public void test_PiNumber(){
        WebElement pi = driver.findElement(By.cssSelector("[value='π']"));
        pi.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(3.14159265359, actual,0);
    }

    @Test
    public void test_SumOfIntegerPositive(){

        WebElement elmt1 = driver.findElement(By.cssSelector("[value='1']"));
        elmt1.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='+']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        int actual =Integer.parseInt(display.getAttribute("value"));

                Assert.assertEquals(3, actual);
    }
    @Test
    public void test_SumOfDoublePositive(){

        WebElement elmt1 = driver.findElement(By.cssSelector("[value='1']"));
        elmt1.click();
        WebElement point=driver.findElement(By.cssSelector("[value='.']"));
        point.click();
        WebElement five=driver.findElement(By.cssSelector("[value='5']"));
        five.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='+']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        point.click();
        WebElement three=driver.findElement(By.cssSelector("[value='3']"));
        three.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(3.8, actual,0);
    }
    @Test
    public void test_clear(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='1']"));
        elmt1.click();
        WebElement clear=driver.findElement(By.cssSelector("[value='C']"));
        clear.click();
        WebElement display=driver.findElementById("display");

        int actual =Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(0, actual);
    }
    @Test
    public void test_SumOfIntegerNegative(){

        WebElement elmt1 = driver.findElement(By.cssSelector("[value='1']"));
        elmt1.click();
        WebElement plusminus=driver.findElement(By.cssSelector("[value='±']"));
        plusminus.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='+']"));
        elmt2.click();
        WebElement firstskope=driver.findElement(By.cssSelector("[value='(']"));
        firstskope.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        plusminus.click();
        WebElement secondskope=driver.findElement(By.cssSelector("[value=')']"));
        secondskope.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        int actual =Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(-3, actual);
    }
    @Test
    public void test_SumOfDoubleNegative(){

        WebElement elmt1 = driver.findElement(By.cssSelector("[value='1']"));
        elmt1.click();
        WebElement point=driver.findElement(By.cssSelector("[value='.']"));
        point.click();
        WebElement five=driver.findElement(By.cssSelector("[value='5']"));
        five.click();
        WebElement plusminus=driver.findElement(By.cssSelector("[value='±']"));
        plusminus.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='+']"));
        elmt2.click();
        WebElement firstskope=driver.findElement(By.cssSelector("[value='(']"));
        firstskope.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        point.click();
        WebElement three=driver.findElement(By.cssSelector("[value='3']"));
        three.click();
        plusminus.click();
        WebElement secondskope=driver.findElement(By.cssSelector("[value=')']"));
        secondskope.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(-3.8, actual,0);
    }
    @Test
    public void test_SumDoubleAndInteger(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='1']"));
        elmt1.click();
        WebElement point=driver.findElement(By.cssSelector("[value='.']"));
        point.click();
        WebElement five=driver.findElement(By.cssSelector("[value='5']"));
        five.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='+']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(3.5, actual,0);
    }
    @Test
    public void test_SumPositiveNegativeNumbers(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='5']"));
        elmt1.click();
        WebElement plusminus=driver.findElement(By.cssSelector("[value='±']"));
        plusminus.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='+']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        int actual =Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(-3, actual);
    }
    @Test
    public void test_MultiplyInteger(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='5']"));
        elmt1.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='*']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        int actual =Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(10, actual);
    }
    @Test
    public void test_MultiplyDoubleInteger(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='5']"));
        elmt1.click();
        WebElement point=driver.findElement(By.cssSelector("[value='.']"));
        point.click();
        WebElement five=driver.findElement(By.cssSelector("[value='5']"));
        five.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='*']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(11, actual,0);
    }
    @Test
    public void test_MultyPositiveNegativeNumbers(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='5']"));
        elmt1.click();
        WebElement plusminus=driver.findElement(By.cssSelector("[value='±']"));
        plusminus.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='*']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        int actual =Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(-10, actual);
    }
    @Test
    public void test_MultiOfDoublePositive(){

        WebElement elmt1 = driver.findElement(By.cssSelector("[value='1']"));
        elmt1.click();
        WebElement point=driver.findElement(By.cssSelector("[value='.']"));
        point.click();
        WebElement five=driver.findElement(By.cssSelector("[value='2']"));
        five.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='*']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        point.click();
        WebElement three=driver.findElement(By.cssSelector("[value='3']"));
        three.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(2.76, actual,0);
    }
    @Test
    public void test_MultiOfDoubleNegative(){

        WebElement elmt1 = driver.findElement(By.cssSelector("[value='1']"));
        elmt1.click();
        WebElement point=driver.findElement(By.cssSelector("[value='.']"));
        point.click();
        WebElement five=driver.findElement(By.cssSelector("[value='2']"));
        five.click();
        WebElement plusminus=driver.findElement(By.cssSelector("[value='±']"));
        plusminus.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='*']"));
        elmt2.click();
        WebElement firstskope=driver.findElement(By.cssSelector("[value='(']"));
        firstskope.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        point.click();
        WebElement three=driver.findElement(By.cssSelector("[value='3']"));
        three.click();
        plusminus.click();
        WebElement secondskope=driver.findElement(By.cssSelector("[value=')']"));
        secondskope.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(2.76, actual,0);
    }
    @Test
    public void test_DivideInteger(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='6']"));
        elmt1.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='/']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        int actual =Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(3, actual);
    }
    @Test
    public void test_MinusIntegerSmollerBigger(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='6']"));
        elmt1.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='-']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        int actual =Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(4, actual);
    }
    @Test
    public void test_MinusIntegerBiggerSmoller(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='2']"));
        elmt1.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='-']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='6']"));
        elmt3.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        int actual =Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(-4, actual);
    }
    @Test
    public void test_MinusIntegerDouble(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='6']"));
        elmt1.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='-']"));
        elmt2.click();
        WebElement elmt7 = driver.findElement(By.cssSelector("[value='1']"));
        elmt7.click();
        WebElement point=driver.findElement(By.cssSelector("[value='.']"));
        point.click();
        WebElement five=driver.findElement(By.cssSelector("[value='5']"));
        five.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(4.5, actual, 0);
    }
    @Test
    public void test_DividePositiveNegativeNumbers(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='6']"));
        elmt1.click();
        WebElement plusminus=driver.findElement(By.cssSelector("[value='±']"));
        plusminus.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='/']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        int actual =Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(-3, actual);
    }
    @Test
    public void test_DivideDoubleIntegerPositive(){

        WebElement elmt1 = driver.findElement(By.cssSelector("[value='6']"));
        elmt1.click();
        WebElement point=driver.findElement(By.cssSelector("[value='.']"));
        point.click();
        WebElement five=driver.findElement(By.cssSelector("[value='4']"));
        five.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='/']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();

        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(3.2, actual,0);
    }
    @Test
    public void test_DivideDoublePositive(){

        WebElement elmt1 = driver.findElement(By.cssSelector("[value='4']"));
        elmt1.click();
        WebElement point=driver.findElement(By.cssSelector("[value='.']"));
        point.click();
        WebElement five=driver.findElement(By.cssSelector("[value='5']"));
        five.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='/']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='1']"));
        elmt3.click();
        point.click();
        five.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(3, actual,0);
    }
    @Test
    public void test_MinusDoublePositive(){

        WebElement elmt1 = driver.findElement(By.cssSelector("[value='2']"));
        elmt1.click();
        WebElement point=driver.findElement(By.cssSelector("[value='.']"));
        point.click();
        WebElement five=driver.findElement(By.cssSelector("[value='5']"));
        five.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='-']"));
        elmt2.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='1']"));
        elmt3.click();
        point.click();
        WebElement three=driver.findElement(By.cssSelector("[value='3']"));
        three.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(1.2, actual,0);
    }
    @Test
    public void test_MinusDoubleNegative(){

        WebElement elmt1 = driver.findElement(By.cssSelector("[value='1']"));
        elmt1.click();
        WebElement point=driver.findElement(By.cssSelector("[value='.']"));
        point.click();
        WebElement five=driver.findElement(By.cssSelector("[value='5']"));
        five.click();
        WebElement plusminus=driver.findElement(By.cssSelector("[value='±']"));
        plusminus.click();
        WebElement elmt2=driver.findElement(By.cssSelector("[value='-']"));
        elmt2.click();
        WebElement firstskope=driver.findElement(By.cssSelector("[value='(']"));
        firstskope.click();
        WebElement elmt3=driver.findElement(By.cssSelector("[value='2']"));
        elmt3.click();
        point.click();
        WebElement three=driver.findElement(By.cssSelector("[value='3']"));
        three.click();
        plusminus.click();
        WebElement secondskope=driver.findElement(By.cssSelector("[value=')']"));
        secondskope.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        double actual =Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(0.8, actual,0);
    }
    @Test
    public void test_GetPercent(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='1']"));
        elmt1.click();
        WebElement percent = driver.findElement(By.cssSelector("[value='%']"));
        percent.click();
        WebElement elmt4=driver.findElement(By.cssSelector("[value='=']"));
        elmt4.click();
        WebElement display=driver.findElementById("display");

        String actual =display.getAttribute("value");

        Assert.assertEquals("0.01", actual);
    }
    @Test
    public void test_getSin(){
            WebElement elmt1 = driver.findElement(By.cssSelector("[value='6']"));
            elmt1.click();
            WebElement sin = driver.findElement(By.cssSelector("[value='sin']"));
            sin.click();
            WebElement display = driver.findElementById("display");

            double actual = Double.parseDouble(display.getAttribute("value"));

            Assert.assertEquals(-0.27941549819892586, actual, 0);

    }
    @Test
    public void test_getCos(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='6']"));
        elmt1.click();
        WebElement sin = driver.findElement(By.cssSelector("[value='cos']"));
        sin.click();
        WebElement display = driver.findElementById("display");

        double actual = Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(0.9601702866503661, actual, 0);

    }
    @Test
    public void test_getTan(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='6']"));
        elmt1.click();
        WebElement sin = driver.findElement(By.cssSelector("[value='tan']"));
        sin.click();
        WebElement display = driver.findElementById("display");

        double actual = Double.parseDouble(display.getAttribute("value"));

        Assert.assertEquals(-0.29100619138474915, actual, 0);

    }
    @Test
    public void test_sqrtPositive(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='6']"));
        elmt1.click();
        WebElement sin = driver.findElement(By.cssSelector("[value='x2']"));
        sin.click();
        WebElement display = driver.findElementById("display");

        int actual = Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(36, actual);
    }
    @Test
    public void test_sqrtNegative(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='6']"));
        elmt1.click();
        WebElement plusminus=driver.findElement(By.cssSelector("[value='±']"));
        plusminus.click();
        WebElement sin = driver.findElement(By.cssSelector("[value='x2']"));
        sin.click();
        WebElement display = driver.findElementById("display");

        int actual = Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(36, actual);
    }
    @Test
    public void test_getSqrt(){
        WebElement elmt1 = driver.findElement(By.cssSelector("[value='36']"));
        elmt1.click();
        WebElement sin = driver.findElement(By.cssSelector("[value='√']"));
        sin.click();
        WebElement display = driver.findElementById("display");

        int actual = Integer.parseInt(display.getAttribute("value"));

        Assert.assertEquals(6, actual);
    }
    @AfterClass
    public static void quitDriver() {
        driver.quit();
    }
}
